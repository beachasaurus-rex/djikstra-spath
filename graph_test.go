package spath

import (
	"math"
	"testing"
)

func TestPoint_Distance(t *testing.T) {
	p1 := Point{0, 0, 0}
	p2 := Point{1, 1, 1}
	expected := math.Sqrt(3)
	if distance := p1.Distance(p2); distance != expected {
		t.Errorf("Distance was incorrect, got: %v, want: %v", distance, expected)
	}

	p1 = Point{3, 4, 0}
	p2 = Point{0, 0, 0}
	expected = 5
	if distance := p1.Distance(p2); distance != expected {
		t.Errorf("Distance was incorrect, got: %v, want: %v", distance, expected)
	}

	p1 = Point{-1, -1, -1}
	p2 = Point{1, 1, 1}
	expected = math.Sqrt(12)
	if distance := p1.Distance(p2); distance != expected {
		t.Errorf("Distance was incorrect, got: %v, want: %v", distance, expected)
	}
}

func TestNode_Equal(t *testing.T) {
	node1 := &Node{point: Point{0, 0, 0}}
	node2 := &Node{point: Point{1, 1, 1}}
	node3 := &Node{point: Point{0, 0, 0}}

	if node1.Equal(node2) {
		t.Errorf("Equal was incorrect, got: true, want: false")
	}

	if !node1.Equal(node3) {
		t.Errorf("Equal was incorrect, got: false, want: true")
	}
}
