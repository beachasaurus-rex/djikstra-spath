package spath

import (
	"testing"
)

func TestPriorityQueue(t *testing.T) {
	pq := make(PriorityQueue, 0)

	// Test Push
	node1 := &Node{dist: 3}
	node2 := &Node{dist: 1}
	node3 := &Node{dist: 2}
	pq.Push(node1)
	pq.Push(node2)
	pq.Push(node3)

	// Test Len
	if len := pq.Len(); len != 3 {
		t.Errorf("Len was incorrect, got: %v, want: %v", len, 3)
	}

	// Test Less
	if less := pq.Less(0, 1); less {
		t.Errorf("Less was incorrect, got: %v, want: %v", less, false)
	}

	// Test Swap
	pq.Swap(0, 1)
	if less := pq.Less(0, 1); !less {
		t.Errorf("Swap was incorrect, Less(0, 1) got: %v, want: %v", less, true)
	}

	// Test Pop
	if pop := pq.Pop().(*Node); !pop.Equal(node3) {
		t.Errorf("Pop was incorrect, got: %v, want: %v", pop, node3)
	}
}
