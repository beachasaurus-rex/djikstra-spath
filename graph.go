package spath

import "math"

type Point struct {
	x, y, z float64
}

func (p Point) Distance(other Point) float64 {
	dx := p.x - other.x
	dy := p.y - other.y
	dz := p.z - other.z
	return math.Sqrt(dx*dx + dy*dy + dz*dz)
}

type Edge struct {
	node   *Node
	weight float64
}

type Node struct {
	point Point
	edges []*Edge
	dist  float64
	index int
	prev  *Node
}

func (n *Node) Equal(other *Node) bool {
	return n.point == other.point
}
