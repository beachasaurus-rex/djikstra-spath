package spath

import (
	"container/heap"
	"errors"
	"math"
)

func ShortestPath(graph map[Point]*Node, start, end Point) ([]Point, error) {
	for _, node := range graph {
		node.dist = math.Inf(1)
		node.prev = nil
		node.index = math.MinInt
	}

	startNode := graph[start]
	startNode.dist = 0

	pq := PriorityQueue{}
	heap.Init(&pq)
	heap.Push(&pq, startNode)

	for pq.Len() > 0 {
		u := heap.Pop(&pq).(*Node)

		if u.point == end {
			var path []Point
			for u != nil {
				path = append([]Point{u.point}, path...)
				u = u.prev
			}
			return path, nil
		}

		for _, edge := range u.edges {
			v := edge.node
			alt := u.point.Distance(v.point) + edge.weight
			if alt < v.dist {
				v.dist = alt
				v.prev = u
				if v.index < 0 {
					heap.Push(&pq, v)
				} else {
					heap.Fix(&pq, v.index)
				}
			}
		}
	}

	return nil, errors.New("no path")
}
