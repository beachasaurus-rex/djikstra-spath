# Djikstra's shortest path algorithm

In this implementation, the cost is equivalent to the Euclidean distance between 2 points in 3D space plus the weight of the connecting edge.

The `ShortestPath()` function returns the following values for the corresponding scenarios, assuming `G` is the finite, unordered set of all points in the graph, `S` is the start point, and `E` is the end point:

1. For S∈G & E∈G, the value returned is a point array that represents the shortest path from S to E.
2. For S=E & S∈G & E∈G, the value returned is a point array containing only S.
3. For S∉G | E∉G, an error is returned.
