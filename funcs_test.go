package spath

import (
	"testing"
)

func TestShortestPath(t *testing.T) {
	// Define some points
	p1 := Point{0, 0, 0}
	p2 := Point{1, 1, 1}
	p3 := Point{2, 2, 2}

	// Define some nodes
	n1 := &Node{point: p1}
	n2 := &Node{point: p2}
	n3 := &Node{point: p3}

	// Define some edges
	e1 := &Edge{node: n2, weight: 1.0}
	e2 := &Edge{node: n3, weight: 1.0}

	// Connect the nodes
	n1.edges = []*Edge{e1}
	n2.edges = []*Edge{e2}

	// Define a graph
	graph := map[Point]*Node{
		p1: n1,
		p2: n2,
		p3: n3,
	}

	t.Run("start equals end", func(t *testing.T) {
		path, err := ShortestPath(graph, p1, p1)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}
		if len(path) != 1 || path[0] != p1 {
			t.Errorf("Expected [%v], got %v", p1, path)
		}
	})

	t.Run("path exists", func(t *testing.T) {
		path, err := ShortestPath(graph, p1, p3)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}
		if len(path) != 3 || path[0] != p1 || path[1] != p2 || path[2] != p3 {
			t.Errorf("Expected [%v, %v, %v], got %v", p1, p2, p3, path)
		}
	})

	t.Run("no path", func(t *testing.T) {
		// Disconnect the nodes
		n1.edges = []*Edge{}
		n2.edges = []*Edge{}

		_, err := ShortestPath(graph, p1, p3)
		if err == nil {
			t.Errorf("Expected error, got nil")
		}
	})
}
